﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuPanel : MonoBehaviour {

    bool menuOpened = false;
    public GameObject menuPanel;

    MenuHandler handler;

    // Use this for initialization
    void Start () {
        handler = (MenuHandler)GameObject.Find("PanelHandler").GetComponent("MenuHandler");
        menuPanel.SetActive(false);
    }

    public void ToggleMenu()
    {
        setMenuVisibility();
    }
    private void setMenuVisibility() {
        if (menuPanel != null) {
            handler.DodajPanelo(menuPanel);
            //menuPanel.SetActive(menuOpened);
        }
    }
	
	// Update is called once per frame
	void Update () {

    }
}
