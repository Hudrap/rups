﻿using UnityEngine;
using System.Collections;
using System;

public class ClockAnimator : MonoBehaviour {
    private const float secondsToDegrees = 360f / 60f;
    public Transform seconds;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        TimeSpan timespan = DateTime.Now.TimeOfDay;
        seconds.localRotation =Quaternion.Euler(0f, 0f, (float)timespan.TotalSeconds * -secondsToDegrees);
    }
}
