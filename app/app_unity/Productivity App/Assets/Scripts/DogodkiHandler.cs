﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TheNextFlow.UnityPlugins;

public class DogodkiHandler : MonoBehaviour {

    List<GUIContent> items;
    List<dogodekItem> itemsData;
    GUIStyle style;
    static float Width = 1920;
    static float Height=1080;
    bool showingPopup = false;

    Vector2 scrollVector;
    void genItems() {
        scrollVector = new Vector2();
        items = new List<GUIContent>();
        itemsData = new List<dogodekItem>();

        items.Add(new GUIContent("12.12.2016 -> Kolokviji!!"));
        items.Add(new GUIContent("13.12.2016 -> Kolokviji!!"));
        items.Add(new GUIContent("14.12.2016 -> Kolokviji!!"));
        items.Add(new GUIContent("15.12.2016 -> Kolokviji!!"));
        items.Add(new GUIContent("16.12.2016 -> Kolokviji!!"));
        items.Add(new GUIContent("17.12.2016 -> Dogodek1"));
        items.Add(new GUIContent("18.12.2016 -> Dogodek2"));
        items.Add(new GUIContent("19.12.2016 -> Dogodek3"));
        items.Add(new GUIContent("20.12.2016 -> Dogodek4"));
        items.Add(new GUIContent("24.12.2016 -> Božič!"));

        itemsData.Add(new dogodekItem("Kolokviji!!", "12.12.2016","Generativne kolokvij"));
        itemsData.Add(new dogodekItem("Kolokviji!!", "13.12.2016", "Izbrani algoritmi kolokvij"));
        itemsData.Add(new dogodekItem("Kolokviji!!", "14.12.2016", "DummyContent"));
        itemsData.Add(new dogodekItem("Kolokviji!!", "15.12.2016", "DummyContent"));
        itemsData.Add(new dogodekItem("Kolokviji!!", "16.12.2016", "DummyContent"));
        itemsData.Add(new dogodekItem("Dogodek1", "17.12.2016", "DummyContent"));
        itemsData.Add(new dogodekItem("Dogodek1", "18.12.2016", "DummyContent"));
        itemsData.Add(new dogodekItem("Dogodek1", "19.12.2016", "DummyContent"));
        itemsData.Add(new dogodekItem("Dogodek1", "20.12.2016", "DummyContent"));
        itemsData.Add(new dogodekItem("Božič", "24.12.2016", "Božič!"));

    }
    public void addItem(dogodekItem item) {
        items.Add(new GUIContent(item.date+" -> "+item.description));
        itemsData.Add(item);
    }
    private void Start()
    {
        genItems();
        style = new GUIStyle();
        style.fontSize = 52;
    }

    void OnGUI()
    {
        //GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(Screen.width / Width, Screen.height / Height, 1));

        GUILayout.BeginHorizontal(GUI.skin.window);
        
        scrollVector = GUILayout.BeginScrollView(scrollVector);
        for (int i = 0; i < items.Count; i++)
        {
            GUILayout.Label(items[i],style);
            bool backPressed = Input.GetKeyDown(KeyCode.Escape);

            if (!showingPopup && !backPressed && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
            {
                showingPopup = true;
                MobileNativePopups.OpenAlertDialog(
                itemsData[i].description,itemsData[i].content,
                "Ok", () => { Debug.Log("Cancel was pressed"); showingPopup = false; });
                //items.Remove(items[i]);
            }
        }
        GUILayout.EndScrollView();
        GUILayout.EndHorizontal();
    }
    

}
public class dogodekItem
{

    public string description;
    public string date;
    public string content;

    public dogodekItem(string desc, string date, string content)
    {
        this.description = desc;
        this.date = date;
        this.content = content;
    }

}
