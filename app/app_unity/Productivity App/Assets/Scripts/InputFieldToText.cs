﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class InputFieldToText : MonoBehaviour {
    public InputField Field;
    public InputField Field2;
    public DogodkiHandler Dhandler;
    public string date;
    public string content;
    public string desc;
    GameObject dogodkiPanel;
    private void Start()
    {
        dogodkiPanel = GameObject.Find("DogodkiPanel");
    }

    public void Click()
    {
        content = Field.text;
        desc = Field2.text;
        DatePicker dp = dogodkiPanel.GetComponent<DatePicker>();
        DateTime mDate = dp.getDate();
        
        if (date == null||date =="") {
            date = "9.12.2016";
        }
        if (Dhandler != null) {
            Dhandler.addItem(new dogodekItem(desc,mDate.ToString() , content));
        }

    }
}
