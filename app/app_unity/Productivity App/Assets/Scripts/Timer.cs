﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.SimpleAndroidNotifications;
using System;

public class Timer : MonoBehaviour {


    // Use this for initialization
    public Text Timertext;
    private float startTime;
    private float ActivityTime;
    private float PauseTime;

    public enum Cikelj {AktivnaFaza,PasivnaFaza};
    public Cikelj cikelj;
    private bool start = false;


    public void setActivityTime(float times) {
        ActivityTime = times;
    }

    public void setPauseTime(float times) {
        PauseTime = times;
    }

    public void StartTimer() {
        start = true;
        startTime = Time.time;
        cikelj = Cikelj.AktivnaFaza;
    }

    public void StopTimer() {
        start = false;
    }
	void Start () {
	}

	// Update is called once per frame
	void Update () {
        if (start)
        {
            if (cikelj == Cikelj.AktivnaFaza)
            {
                if (ActivityTime <= 0 && start)
                {   // change to pause!
                    //start = false;
                    notifyActivitySwitch();
                    cikelj = Cikelj.PasivnaFaza;
                }
                ActivityTime = ActivityTime - Time.deltaTime;
                string minutes = ((int)ActivityTime / 60).ToString();
                string seconds = (ActivityTime % 60).ToString("f2");
                Timertext.text = minutes + ":" + seconds;
            }
            else {
                if (PauseTime <= 0 && start){  
                     
                    start = false;
                    notifyActivitySwitch();
                }
                PauseTime = PauseTime - Time.deltaTime;
                string minutes = ((int)PauseTime / 60).ToString();
                string seconds = (PauseTime % 60).ToString("f2");
                Timertext.text = minutes + ":" + seconds;
            }
        }
        else {
            Timertext.text = "00:00.0";
        }
    }

    void notifyActivitySwitch() {
        NotificationParams notificationParams;
        if (cikelj == Cikelj.AktivnaFaza)
        {
            notificationParams = new NotificationParams
            {
                Id = UnityEngine.Random.Range(0, int.MaxValue),
                Delay = TimeSpan.FromSeconds(5),
                Title = "Zamejaj aktivnost!",
                Message = "Čas aktivnosti je potekel!",
                Ticker = "Ticker",
                Sound = true,
                Vibrate = true,
                Light = true,
                SmallIcon = NotificationIcon.Message,
                SmallIconColor = new Color(0, 0.5f, 0),
                LargeIcon = "app_icon"
            };
        }
        else {
            notificationParams = new NotificationParams
            {
                Id = UnityEngine.Random.Range(0, int.MaxValue),
                Delay = TimeSpan.FromSeconds(5),
                Title = "Zamejaj aktivnost!",
                Message = "Čas pavze je potekel!",
                Ticker = "Ticker",
                Sound = true,
                Vibrate = true,
                Light = true,
                SmallIcon = NotificationIcon.Message,
                SmallIconColor = new Color(0, 0.5f, 0),
                LargeIcon = "app_icon"
            };
        }

        NotificationManager.SendCustom(notificationParams);
    }
}
