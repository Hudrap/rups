﻿using UnityEngine;
using System.Collections;

public class MenuHandler : MonoBehaviour {

    private static Stack VsePanele;
    private static GameObject AktivnaPanela = null;
    private static int  indeksPanele = 0;
    private static Time prejsni_cas;
    private static MenuHandler instance;

	// Use this for initialization
	void Start () {
        VsePanele = new Stack();
	}
    public void DodajPanelo(GameObject panela) {
        if (!VsePanele.Contains(panela)) {
            panela.SetActive(true);
            VsePanele.Push(panela);
        }
    }
    public static void prejsnaPanela() {
        ((GameObject)VsePanele.Peek()).SetActive(false);
        VsePanele.Pop();
        //((GameObject)VsePanele.Peek()).SetActive(true);
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (VsePanele.Count > 0)
                prejsnaPanela();
            return;
        }
        
    }
	
}




